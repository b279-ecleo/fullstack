import { Navigate } from "react-router-dom";
import UserContext from "../UserContext"
import { useContext, useEffect } from "react"

export default function Logout(){

	// localStorage.clear();

	// Consume user context object and destructure it to access the user state and unsetUser function

	const {unsetUser, setUser} = useContext(UserContext);

	// Clear the localStorage
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null,
			email: null,
			token: null
		})
	})

	// Navigate back to login
	return(
		<Navigate to="/login" />
		);
}