import { Carousel, Image } from 'react-bootstrap';


export default function HomeCarousel() {
  return (
    <div>
      <Carousel className="vh-75 w-100 d-inline-block shadow" fade>
        <Carousel.Item>
          <div className="image-container">
            <Image
              className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
              src={require('../images/carousel1.jpeg')}
              alt="First slide"
            />
          </div>

        </Carousel.Item>
        <Carousel.Item>
          <div className="image-container">
            <Image
              className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
              src={require('../images/carousel2.jpeg')}
              alt="Second slide"
            />
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="image-container">
            <Image
              className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
              src={require('../images/carousel3.jpeg')}
              alt="Third slide"
            />
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="image-container">
            <Image
              className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
              src={require('../images/carousel4.jpeg')}
            />
          </div>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}