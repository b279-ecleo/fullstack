import { Link } from "react-router-dom";
import { Row, Col, Button, Image } from "react-bootstrap";


export default function HomeBanner({bannerProp}){
    const {title, content, destination, label, image} = bannerProp;
    return(
        <div className="vh-25 banner-bg text-light d-flex align-items-center justify-content-center mb-5">
        <Row>
            <Col className="text-center d-flex flex-column justify-content-center align-items-center " lg={12}>
                <h2 className="hb-h2">{title}</h2>
                <p className="hb-p">{content}</p>
                <Button className="w-50 btn-outline-light btn-fnt" as = {Link} to={destination} variant="">{label}</Button>
            </Col>
        </Row>
        </div>
    )
}