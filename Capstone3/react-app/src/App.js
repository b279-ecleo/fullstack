import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap";
import { UserProvider } from "./UserContext";
import { useState, useEffect } from "react";
import React from 'react';
// IMPORT PAGES
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from './pages/Login';
import Logout from './pages/Logout';
import Dashboard from './pages/Dashboard';
import ErrorPage from './pages/ErrorPage';
import AllProducts from './pages/AllProducts';
import Products from './pages/Product';
import CheckOut from './pages/CheckOut';
// IMPORT COMPONENTS
import AppNavbar from "./components/AppNavbar";
import Swal from "sweetalert2";



function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`http://localhost:4420/user/details`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        console.log(" success");
        setUser({
            id: data._id,
            firstName : data.firstName,
            lastName : data.lastName,
            email : data.email,
            mobileNo : data.mobileNumber,
            isAdmin: data.isAdmin
        });
      }
      else{
        setUser({
          
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])

  return (
    <>
    <div className="overflow-hidden">
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid className='p-0 m-0'>
          <Routes>
          <Route exact path="/" element={<Home />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/dashboard" element={<Dashboard />} />
            <Route exact path="/products/allProducts" element={<AllProducts/>} />
            <Route exact path="/products" element={<Products/>} />
            <Route exact path="/product/:productId" element={<CheckOut />} />
            <Route exact path="*" element={<ErrorPage />} />
          </Routes>
        </Container>
        </Router>
    </UserProvider>
    </div>
    </>
    

  );
}

export default App;
