import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import HomeBanner from "../components/HomeBanner";


export default function Home(){


    const { user } = useContext(UserContext);
    const data = {
        title: "Shopping Mall",
        content: "We got it all for you!",
        destination: "/products",
        label: "Shop Now!",
    }

    return(
        <>
        {
        (user.isAdmin)
        ?
        <Navigate to="/dashboard" />
        :
        <div className="p-5">
        <HomeBanner bannerProp={data}/>
        </div>
        }
        </>

    )
}