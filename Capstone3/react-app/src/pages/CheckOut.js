import { useState, useEffect, useContext } from "react";
import { Card, Button, Col, Form } from "react-bootstrap";
import { useParams, useNavigate, Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import HomeBanner from "../components/HomeBanner";
import UserContext from "../UserContext";

export default function ProductView() {
  const data = {
    title: "YOU ARE ABOUT TO MAKE A TRANSACTION",
    content: "We accept COD Nationwide!",
    destination: "/products",
    label: "Other Products",
    // image: {logo}
  };

    const ProductDetails = () => {
    const { user } = useContext(UserContext);
    const { productId } = useParams();
    const navigate = useNavigate();

    const [productBrand, setBrand] = useState("");
    const [productName, setName] = useState("");
    const [productModel, setModel] = useState("");
    const [description, setDescription] = useState("");
    const [stocks, setStocks] = useState("");
    const [imgSource, setImg] = useState("");
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState("");

    useEffect(() => {
      console.log(productId);

      fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          // Add any additional headers if required
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setBrand(data.productBrand);
          setName(data.productName);
          setModel(data.productModel);
          setImg(data.imgSource);
          setDescription(data.description);
          setPrice(data.price);
          setStocks(data.stocks); // Set the stocks value
        })
        .catch((error) => {
          console.error(error);
          // Handle error
        });
    }, [productId]);

    const buy = () => {
      console.log("Product ID:", productId); // Add this line to log the product ID
    
      fetch(`${process.env.REACT_APP_API_URL}/user/checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          productId: productId,
          quantity: quantity,
        }),
      })
        .then((res) => {
          if (!res.ok) {
            throw new Error(res.statusText);
          }
          return res.json();
        })
        .then((data) => {
          console.log(data);
          if (data && data.error) {
            throw new Error(data.error);
          }
          Swal.fire({
            title: "PRODUCT SOLD!",
            icon: "success",
            text: `Thank you for purchasing ${productName}.
            A total of Php ${data.totalAmount} was deducted from your E-wallet.`,            
          });
          setStocks(data.stocks); // Update the stocks value from the response
          navigate("/products");
        })
        .catch((error) => {
          console.error(error);
          Swal.fire({
            title: "Oops! Something went wrong",
            icon: "error",
            text: error.message || "An error occurred. Please try again.",
          });
        });
    };
        
    return (
      <>
        {user.isAdmin ? (
          <Navigate to="/dashboard" />
        ) : (
          <div className="p-5">
            <HomeBanner bannerProp={data} />
            <div className="d-flex align-items-center justify-content-center flex-column">
              <Col className="my-2 " xs={12} md={12} lg={6}>
                <Card className="my-3 w-100  card-height shadow card-border shadow-md card-bg ">
                  <Card.Img className="product-img-cover" src={imgSource} />
                  <Card.Header className="py-3 my-3 ">
                    <Card.Title>
                      {productBrand} - {productName}
                    </Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>Php {price}</Card.Text>
                    <Card.Subtitle>Stocks</Card.Subtitle>
                    <Card.Text>{stocks}</Card.Text>
                    <Card.Subtitle>Quantity</Card.Subtitle>
                    <Form.Control
                      className="w-25 my-2"
                      type="number"
                      required
                      onChange={(e) => setQuantity(e.target.value)}
                      value={quantity}
                    />
                  </Card.Body>
                  <div className="text-center">
                    <Card.Footer>
                      {user.id !== null ? (
                        <Button
                          id="checkOutBtn"
                          className="w-50 btn-dark my-3 shadow"
                          onClick={() => buy()}
                        >
                          CHECK OUT
                        </Button>
                      ) : (
                        <Button
                          className="w-50 btn-danger my-3 shadow"
                          as={Link}
                          to={`/login`}
                        >
                          LOGIN TO BUY
                        </Button>
                      )}
                    </Card.Footer>
                  </div>
                </Card>
              </Col>
            </div>
          </div>
        )}
      </>
    );
  };

  return <ProductDetails />;
}