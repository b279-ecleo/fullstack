import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";
import { Row } from "react-bootstrap";

export default function Products() {
  const data = {
    title: "Shopping Mall",
    content: "Worldwide provider for your needs and wants!",
    destination: "/products",
    label: "Shop Now!",
    // image: {logo}
  };

  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const unarchivedProducts = data.filter((product) => product.isActive === true); // Filter out archived products
        setProducts(
          unarchivedProducts.map((product) => {
            return <ProductCard key={product._id} props={product} />;
          })
        );
      });
  }, []);

  return user.isAdmin ? (
    <Navigate to="/dashboard" />
  ) : (
    <>
      <div className="p-5 colr-bg">
        <h1 className="prod-h1">Products Catalog</h1>
        <Row className="my-3 text-dark h-100">{products}</Row>
      </div>
    </>
  );
}
