import { useContext } from "react"
import UserContext from "../UserContext";
import {Container, Col, Row} from "react-bootstrap"
import { Navigate } from "react-router-dom";
import AppSideNav from "../components/AppSideNav";
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
import HomeBanner from "../components/HomeBanner.js";

export default function Dashboard(){
     const { user } = useContext(UserContext);
    console.log(user);

    const data = {
        title: "Dashboard Menu",
        content: "Let's get started!",
        destination: "/products/allProducts",
        label: "PRODUCTS",
        // image: {logo}
    }


    return(
        <>
        {
        (user.isAdmin )
        ?
        <Container fluid className="w-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white vh-100 pt-3 m-0 shadow" xs={12} md={2} lg={2}>
        <AppSideNav/>
        </Col>
        <Col className="colr-bg vh-100 m-0 p-5" xs={12} md={10} lg={10}>
        <HomeBanner bannerProp={data}/>
        </Col>

        </Row>
     </Container>
        :
        <Navigate to={"/"}/>
        }
   </>
    )
}