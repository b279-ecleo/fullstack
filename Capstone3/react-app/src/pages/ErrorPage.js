import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import HomeBanner from "../components/HomeBanner";

export default function ErrorPage(){


    const { user } = useContext(UserContext);
    const data = {
        title: "404: ERROR!",
        content: "The product or page you are looking cannot be found.",
        destination: "/",
        label: "Home",
        // image: {logo}
    }

    return(
        <>
        {
        (user.isAdmin)
        ?
        <Navigate to="/dashboard" />
        :
        <div className="p-5">
            <HomeBanner bannerProp={data}/>
        </div>
        }
        </>

    )
}